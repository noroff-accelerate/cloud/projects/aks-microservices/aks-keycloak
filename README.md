# Keycloak

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Deployment configuration for Keycloak and supporting applications

## Table of Contents

- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Usage

Create a new namespace

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: keycloak
  namespace: keycloak
```

Create a new secret containing the password

```sh
k create secret generic -n keycloak keycloak-db-postgresql --from-literal=postgresql-password=<your super secret password>
```

Deploy using Fleet

```yaml
kind: GitRepo
apiVersion: fleet.cattle.io/v1alpha1
metadata:
  name: keycloak
  namespace: fleet-local
spec:
  repo: https://gitlab.com/noroff-accelerate/cloud/projects/aks-microservices/aks-keycloak
  branch: master
  targetNamespace: keycloak
  paths:
    - keycloak
    - postgres
```

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Noroff Accelerate AS
